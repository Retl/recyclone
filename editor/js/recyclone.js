Recyclone = function () {
	this.uiActionHistory = new UIActionHistory();
	this.levelToId = this.defaultLevelToID();


	this.fileAccessSupported = false;

	this.filePicker = document.createElement("input");
	this.selectedFile = {};
	this.filePicker.type = "file";
	
	//this.filePicker.addEventListener('change', this.processLoadFile, false);
	this.filePicker.addEventListener('change', this.processLoadFile, false);

	this.fileReader = new FileReader();
	//this.fileReader.addEventListener('onload', this.processLoadFileComplete, false);
	this.fileReader.onload = this.processLoadFileComplete;

	this.activeObjects = [];
	this.selectedObjects = [];
};


Recyclone.prototype.detectFileAccess = function () {
	// Check for the various File API support.
	if (window.File && window.FileReader && window.FileList && window.Blob) {
		// Great success! All the File APIs are supported.
		this.fileAccessSupported = true;
	} else {
		alert('The File APIs are not fully supported in this browser.');
	}
}

Recyclone.prototype.load = function () {
	Recyclone.prototype.detectFileAccess();
	if (this.fileAccessSupported) {
		console.log('Do load here...');
		this.filePicker.value = null;
		this.filePicker.click();
	}
}

Recyclone.prototype.processLoadFile = function (evt) {
	if (recyclone.filePicker.value == ""
		|| evt.target.files.length == 0) { return; }

	var files = evt.target.files;
	console.log("Num files: " + evt.target.files.length);
	console.log(evt.target)
	//console.log(evt);
	recyclone.selectedFile = files[0];
	console.log(recyclone.selectedFiles);
	console.log(this);
	var fileExt = recyclone.filePicker.value.split('.').pop().toLowerCase();
	if (fileExt == 'lvl') {
		recyclone.fileReader.readAsText(recyclone.selectedFile);
	}
	else if (fileExt == 'png') {
		var pngName = recyclone.selectedFile.name.substring(0, recyclone.selectedFile.name.length - fileExt.length - 1);
		recyclone.remapImage(null, pngName);
	}
	else {
		recyclone.fileReader.readAsText(recyclone.selectedFile);
	}

}

Recyclone.prototype.processLoadFileComplete = function (e) {
	fileData = e.target.result;
	//console.log("File Data: " + fileData);
	lvlJSON = JSON.parse(fileData);
	recyclone.levelName = lvlJSON.levelName || recyclone.levelToId[lvlJSON.levelId] || 'RecyclonED';
	recyclone.activeObjects = lvlJSON.activeObjects;
	recyclone.fpVersion = lvlJSON.fpVersion;
	recyclone.fpSize = lvlJSON.fpSize;
	recyclone.width = lvlJSON.width;
	recyclone.height = lvlJSON.height;
	recyclone.game.world.setBounds(0, 0, recyclone.width, recyclone.height);
	grp_activeObjects.removeAll();
	if (recyclone.activeObjects.length > 0) {
		for (var ao of recyclone.activeObjects) {
			var spr = game.add.sprite(ao.x, ao.y, 'warning');
			spr.ao = ao;
			spr.inputEnabled = true;

			spr.events.onInputDown.add(function () {
				// Left Click.
				if (game.input.activePointer.button == 0) {
				recyclone.selectedObjects = [this];
				// TODO: Move this to main.js's input onDown, or as some detail of all selection-handling.
				recyclone.selectedObjects[0].previousPosition = new Phaser.Point(recyclone.selectedObjects[0].position.x, recyclone.selectedObjects[0].position.y);
				console.log(recyclone.selectedObjects[0]);
				console.log('Selected object\'s image: ' + recyclone.selectedObjects[0].ao.image);
				console.log('Selected object\'s addrIntCreateFun: ' + recyclone.selectedObjects[0].ao.addrIntCreateFun);
				}
			}, spr);

			spr.events.onInputUp.add(function () {
				// TODO: Move this to main.js's input onUp.
				recyclone.uiActionHistory.addAction(this.position.set, [this.previousPosition.x, this.previousPosition.y], this); 
				// recyclone.selectedObjects = [];
				// selected = null;
			}, spr);

			spr.input.enableDrag(true);
			grp_activeObjects.add(spr);

			if (typeof ao['image'] != 'undefined') {
				if (!game.cache.checkImageKey(ao['image'])) {
					game.load.image(ao['image'], '../data/' + recyclone.fpVersion + '/images/' + ao['image'] + '.png');
				}
			}
			/*
			if (typeof ao['images'] != 'undefined' && ao['images'].length > 0) {
				ao['image'] = ao['images'][0];
				//game.load.image(ao['image'], '../data/' + recyclone.fpVersion + '/images/' + ao['image'] + '.png');
			}*/
		}
		game.load.start();
	}
	//recyclone.filePicker.value = "";
	console.log('recyclone.levelName: ' + recyclone.levelName);
}

Recyclone.prototype.save = function () {
	console.log("Now saving...")
	var jsonOut = {};
	jsonOut.levelName = recyclone.levelName || 'RecyclonED';
	jsonOut.activeObjects = recyclone.activeObjects;
	jsonOut.width = recyclone.width; // TODO: FIXME
	jsonOut.height = recyclone.height; // TODO: FIXME
	jsonOut.fpVersion = recyclone.fpVersion; // TODO: FIXME
	jsonOut.fpSize = recyclone.fpSize; // TODO: FIXME
	var saveText = JSON.stringify(jsonOut, null, 2);
	var saveBlob = new Blob([saveText], { type: 'text/plain' });
	this.downloadURI(URL.createObjectURL(saveBlob), (jsonOut.levelName + '.lvl'));
}

Recyclone.prototype.downloadURI = function (uri, name) {
	var link = document.createElement("a");
	link.download = name;
	link.href = uri;
	link.click();
}

Recyclone.prototype.createURI = function (saveString) {
	return ("data:application/octet-stream;charset=utf8," + saveString);
}


Recyclone.prototype.reloadAOTex = function () {
	console.log("Level image load complete! Assigning to objects...")
	grp_activeObjects.forEach(function (spr) {
		if (typeof spr.ao != 'undefined'
			&& typeof spr.ao.image != 'undefined') {
			spr.loadTexture(spr.ao.image);
		}
	});
	console.log('DONE!');
}

// TODO: MAKE THIS MAKE SENSE ALSO REFACTOR EVERYTHING EVER
Recyclone.prototype.loadImage = function () {
	Recyclone.prototype.detectFileAccess();
	if (this.fileAccessSupported) {
		console.log('Do load here...');

		this.filePicker.click();
	}
}
Recyclone.prototype.remapImage = function (e, spcFile) {
	recyclone.selectedObjects.forEach(function (spr, ind, arr) {
		if (spr && spr.ao) {
		var oldImg = spr.ao.image;
		var createFunctionAbsolute = spr.ao.addrIntCreateFun;
		if (!spcFile) {
			this.filePicker.click();
			return;
		}
		else if (spcFile) {
			newImg = spcFile;
		}
		if (newImg) {
			spr.ao.image = newImg;

			// TODO: Loop in a loop. Refactor?
			for (var ao of recyclone.activeObjects) {
				if (ao.addrIntCreateFun == spr.ao.addrIntCreateFun) {
					ao.image = newImg;
				}
			}

			if (game.cache.checkImageKey(spr.ao.image)) {
				recyclone.reloadAOTex();
			}
			else {
				game.load.image(spr.ao.image, '../data/' + recyclone.fpVersion + '/images/' + selected.ao.image + '.png');
				game.load.start();
			}
		}
	}
	});
	
}

Recyclone.prototype.loadImgDict = function (e) {
	var strImgs = fs.readFile('../data/' + recyclone.fpVersion + '/images/images.dict', null, function (err, fileData) {
		recyclone.imgDict = JSON.parse(strImgs);
	});
}

Recyclone.prototype.defaultLevelToID = function () {
	return ([
		"Intro",
		"Black Load",
		"Title Screen",
		"Main Menu",
		"(unused)",
		"Time Attack",
		"Character Select",
		"Black Load",
		"Update Records",
		"Unlocked!",
		"Gallery",
		"Adventure Stage Select",
		"Mahjong",
		"Bonus Stage",
		"Lilac's Treehouse (Day)",
		"Lilac's Treehouse (Night)",
		"Aqua Tunnel 1",
		"Aqua Tunnel 2",
		"Aqua Tunnel 3",
		"Aqua Tunnel 4",
		"Dragon Valley 1",
		"Dragon Valley 2",
		"Dragon Valley 3",
		"Dragon Valley 4",
		"Relic Maze 1",
		"Relic Maze 2",
		"Relic Maze 3",
		"Relic Maze 4",
		"Relic Maze 5",
		"Fortune Night 1",
		"Fortune Night 2",
		"Fortune Night 3",
		"Fortune Night 4",
		"Fortune Night 5",
		"Fortune Night 6",
		"Sky Battalion Hub",
		"Sky Battalion 1 - Fire Ship",
		"Sky Battalion 2 - Metal Ship",
		"Sky Battalion 3 - Earth Ship",
		"Jade Creek 1",
		"Jade Creek 2",
		"Jade Creek 3",
		"Jade Creek 4",
		"Jade Creek 5",
		"Thermal Base 1",
		"Thermal Base 2",
		"Thermal Base 3",
		"Thermal Base 4",
		"Thermal Base 5",
		"Thermal Base 6",
		"Pangu Lagoon 1",
		"Pangu Lagoon 2",
		"Pangu Lagoon 3",
		"Pangu Lagoon 4",
		"Pangu Lagoon 5",
		"Pangu Lagoon 6",
		"Trap Hideout 1",
		"Trap Hideout 2",
		"Trap Hideout 3",
		"Battle Glacier 1",
		"Battle Glacier 2",
		"Battle Glacier 3",
		"Battle Glacier 4",
		"Battle Glacier 5",
		"Battle Glacier 6",
		"Final Dreadnought 1",
		"Final Dreadnought 2",
		"Final Dreadnought 3",
		"Final Dreadnought 4",
		"Final Dreadnought 5",
		"Final Dreadnought 6",
		"Final Dreadnought 7",
		"Final Dreadnought 8",
		"Shang Mu Academy",
		"REDACTED",
		"REDACTED",
		"Shang Tu Dojo",
		"Continue?",
		"Scene - Dragon Valley",
		"Scene - Relic Maze",
		"REDACTED",
		"Scene - Fortune Night",
		"REDACTED",
		"Scene - Thermal Base",
		"Scene - Battle Glacier",
		"Scene - Indoor Rooms",
		"Scene - Space",
		"Schmup - Battle Glacier",
		"Credits",
		"Weapon Select",
		"REDACTED"
	]);
};

Recyclone.prototype.selectAllActiveObjects = function () {
	grp_activeObjects.forEach(function (spr) {
		recyclone.selectedObjects.push(spr);
	});
};

Recyclone.prototype.selectedToTop = function () {
		for (var so of this.selectedObjects) {
		so.bringToTop();
		}
};

Recyclone.prototype.selectedToBottom = function () {
		for (var so of this.selectedObjects) {
		so.sendToBack();
		}
};

Recyclone.prototype.startBoxSelect = function () {
		mouseMode = 'boxSelect';
};

Recyclone.prototype.deleteActiveObject = function (ao, spr) {
	if (spr) {
		spr.kill();
	}
	if (ao) {
		for (var i = 0; i < this.activeObjects.length; i++) {
			if (this.activeObjects[i].addr == ao.addr) {
				this.activeObjects.splice(i, 1);
				break;
			}
		}
	}
};

Recyclone.prototype.deleteSelectedObjects = function () {
	if (recyclone.selectedObjects && recyclone.selectedObjects.length > 0) {
		for (var spr of recyclone.selectedObjects) {
			this.deleteActiveObject(spr.ao, spr);
		}
	}
};

Recyclone.prototype.randomizeSelectedObjects = function () {
	if (recyclone.selectedObjects && recyclone.selectedObjects.length > 0) {
		for (var spr of recyclone.selectedObjects) {
			this.moveToRandomPosition(spr);
		}
	}
};

Recyclone.prototype.moveToRandomPosition = function (spr) {
	if (spr) {
		var nx = 32 + Math.floor((Math.random() * game.world.width) - 64);
		var ny = 32 + Math.floor((Math.random() * game.world.height) - 64);
		spr.position.set(nx, ny);
	}
};