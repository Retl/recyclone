UIAction = function (actFun, actArgs, actContext) {
	this.actFun = actFun || function () { console.log('Default UI Action does nothing.'); };
	this.actArgs = actArgs || [];
	this.actContext = actContext || this.actFun;
};

UIAction.prototype.exec = function () {
	console.log('this.actFun: ' + this.actFun);
	console.log('this.actArgs: ' + this.actArgs);
	console.log('this.actContext: ' + this.actContext);
	console.log('this.actContext\' Proeprties: ' + Object.getOwnPropertyNames(this.actContext));
	
	this.actFun.apply(this.actContext, this.actArgs);
};

UIActionHistory = function () {
	this.maximumHistorySize = 100;
	this.undoActions = [];
	this.redoActions = [];
};

UIActionHistory.prototype.addAction = function (actFun, actArgs, actContext) {
	this.undoActions.push(new UIAction(actFun, actArgs, actContext));
	if (this.undoActions.length > this.maximumHistorySize) {
		this.undoActions.shift();
	}
};

UIActionHistory.prototype.undo = function () {
	console.log('Attempting undo');
	if (this.undoActions.length > 0) {
		var undoMe = this.undoActions.pop();
	}
		if (!undoMe) {
		console.log('Attempted to undo non-UIAction, but type was ' + typeof undoMe + '. Something is wrong.');
		}
		else {
		this.redoActions.push(undoMe);
		if (this.redoActions.length > this.maximumHistorySize) {
			this.redoActions.shift();
		}
		undoMe.exec();
	}
};