var Input = function () {
	this.pad;

	this.buttonA;
	this.buttonB;
	this.buttonX;
	this.buttonY;
	this.buttonDPadLeft;
	this.buttonDPadRight;
	this.buttonDPadUp;
	this.buttonDPadDown;
	
	this.cursors = game.input.keyboard.createCursorKeys();
	
	this.keyAtk = game.input.keyboard.addKey(Phaser.Keyboard.X);
	// this.keyJump = game.input.keyboard.addKey(Phaser.Keyboard.Z);
	this.keyJumpAlt = game.input.keyboard.addKey(Phaser.Keyboard.SPACE);
	
	this.keyLoad = game.input.keyboard.addKey(Phaser.Keyboard.O);
	this.keySave = game.input.keyboard.addKey(Phaser.Keyboard.P);
	
	this.keyRemapImage = game.input.keyboard.addKey(Phaser.Keyboard.L);
	
	this.keyRandomPosition = game.input.keyboard.addKey(Phaser.Keyboard.TILDE);
	
	this.keySelToTop = game.input.keyboard.addKey(Phaser.Keyboard.PAGE_UP);
	this.keySelToBottom = game.input.keyboard.addKey(Phaser.Keyboard.PAGE_DOWN);
	
	this.keyUndo = game.input.keyboard.addKey(Phaser.Keyboard.Z);
	
	this.keyAllSelect = game.input.keyboard.addKey(Phaser.Keyboard.A);
	this.keyBoxSelect = game.input.keyboard.addKey(Phaser.Keyboard.B);
	this.keyDelete = game.input.keyboard.addKey(Phaser.Keyboard.DELETE);
	
	this.keyScale = game.input.keyboard.addKey(Phaser.Keyboard.S);
	this.keyFullscreen = game.input.keyboard.addKey(Phaser.Keyboard.F);
	
	this.bindPresses();
	this.bindReleases();
	
	this.bindJoyPresses();
	this.bindJoyReleases();
};

Input.prototype.FIXME = function () {
	game.input.gamepad.start();
	pad = game.input.gamepad.pad1;
	pad.addCallbacks(this, { onConnect: addButtons });
}


Input.prototype.bindPresses = function () {
	this.keyLoad.onDown.add(function () {recyclone.load();}, this);
	this.keySave.onDown.add(function () {recyclone.save();}, this);
	this.keyUndo.onDown.add(function () {recyclone.uiActionHistory.undo();}, recyclone);
	this.keySelToTop.onDown.add(function () {recyclone.selectedToTop();}, recyclone);
	this.keyRandomPosition.onDown.add(function () {recyclone.randomizeSelectedObjects();}, recyclone);
	this.keySelToBottom.onDown.add(function () {recyclone.selectedToBottom();}, recyclone);
	this.keyAllSelect.onDown.add(function () {recyclone.selectAllActiveObjects();}, recyclone);
	this.keyBoxSelect.onDown.add(function () {recyclone.startBoxSelect();}, recyclone);
	this.keyDelete.onDown.add(function () {recyclone.deleteSelectedObjects();}, recyclone);
	

	this.keyScale.onDown.add(ld.changeScaleMode, player);
	this.keyFullscreen.onDown.add(ld.goFullscreen, this);
};

Input.prototype.bindReleases = function () {
	this.keyRemapImage.onUp.add(function () {recyclone.remapImage();}, recyclone);
};

Input.prototype.bindJoyPresses = function () {

};

Input.prototype.bindJoyReleases = function () {

};