var Level = function (addr, name, length, width) {
	this.addr = addr;
  this.moniker = name;
  this.length = length;
  this.width = width;
	
	//return this;
}

Level.prototype.exportJSON = function () {
  return {
    levelName: "Unnamed Level",
    length: 640,
    width: 480,
    objects: [
      null,
      null,
      null
    ]
  };
};