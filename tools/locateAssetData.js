var fs = require('fs');
var zlib = require('zlib');
var Jimp = require("jimp");

const OFFSETS_START = 0x83fa;
const IMAGE_COUNT = 16893;

var bytetools = require('../editor/js/bytetools.js');
var leveldata = require('./levelData.js');

var frameObjectDataOffsets = [];
var imageMatches = [];
var pngs = [];
var pngsIndex = 0;

var assetFileIn = '../data/orig/Assets.dat';
var filenameImageDict = '../data/dictionary/images-1.20.4.dict';
var ext = '.dat';
ext = '.imgs';

var pathToData = '../data/';
var pathImages = 'images/';
var filenamePrepend = 'Assets-';
var imgExt = '.png';

var fullData = '';

var filename = assetFileIn;
if (process.argv[2]) {
  filename = process.argv[2];
}
// var filenameOut = 'fp_turbo.exe'
var fileVersion = "unknown";
var stats = fs.statSync(filename);
var fileSizeInBytes = stats["size"];
console.log("Filesize is: " + fileSizeInBytes);
if (fileSizeInBytes == 30750285) { fileVersion = "steam-1-20-4"; }

console.log(fileVersion);
var filenameOut = pathToData + filenamePrepend + fileVersion + ext;
var pathDataOut = pathToData + fileVersion + '/';
var pathImageOut = pathToData + fileVersion + '/' + pathImages;

var segmentOffsetStart = 0;
var segmentOffsetEnd = 0;

var chunkCount = 1;

var inflate = zlib.createInflate();
var inflateRaw = zlib.createInflateRaw();
var readStream;
var writeStream;
var dictStream;

var imgDictStr = '';
var imgDict = '';

var main = function () {

  dictStream = new fs.ReadStream(filenameImageDict);
  readStream = new fs.ReadStream(filename);
  writeStream = new fs.WriteStream(filenameOut);

  dictStream.on('data', function (chk) { imgDictStr += chk });
  dictStream.on('end', function () {
    imgDict = JSON.parse(imgDictStr);
    fileVersion = imgDict.fpVersion;
    console.log('Finished reading image dictionary: ' + imgDict);
  });

  readStream.setEncoding('hex');
  readStream.on('open', function (chunk) {
  });
  
  // Actually read the file.
  readStream.on('data', function (chunk) {
    fullData += chunk;
  });

  // After getting the entire body.
  readStream.on('end', function () {
    console.log('END. Final running data length in Bytes: ' + (fullData.length / 2));
    console.log('Image Dictionary currently contains: ' + imgDict);

    dumpAllImages();

    var jsonOut = {};
    jsonOut.assetsVersion = fileVersion;
    jsonOut.assetsSize = fileSizeInBytes;
    jsonOut.images = imageMatches;

    writeStream.write(JSON.stringify(jsonOut, null, 2), 'utf8');
    writeStream.end();


  });

  readStream.on('error', function () {
    console.log('@@@AN ERROR OCCURED@@@');
  });

  function displaySegmentContents() {
    if (typeof segmentOffsetStart !== 'undefined' && typeof segmentOffsetEnd !== 'undefined') {
      fs.read(filename, 'hex');
    }
    else {
      console.log('Unable to display segment content: At least one of the offsets were undefined.');
    }
  }
};

function dumpAllImages() {
  var imgHandle;

  createPath(pathDataOut);
  createPath(pathDataOut + 'mod_levels');
  createPath(pathImageOut);

  for (imgId = 0; imgId < IMAGE_COUNT; imgId++)
  {
    var img = createImgObjAuto(imgId);
    var buf = new Buffer(img.data, 'hex');
    var puffedBuf = zlib.inflateSync(buf);
    writeImageToFile(img, puffedBuf, fileVersion);
    imageMatches.push(img)
  }
  startProcessingPngs();
  console.log('Done. Images have been dumped to "' + pathImageOut + '/');
};

function createImgObj(currentImageStartIndex, indexOfDataSizeInt, indexData, lll, targetString, imgId) {
  var img;
  try {
    img = {
      imgId: imgId,
      targetString: targetString,
      aob: fullData.substring(currentImageStartIndex, indexOfDataSizeInt),
      length: bytetools.hexIntToInt(fullData.substring(indexOfDataSizeInt, indexData)),
      lengthHex: fullData.substring(indexOfDataSizeInt, indexData),
      width: bytetools.hexShortToInt(fullData.substring(currentImageStartIndex, currentImageStartIndex + 4)),
      height: bytetools.hexShortToInt(fullData.substring(currentImageStartIndex + 4, currentImageStartIndex + 8)),
      hotX: bytetools.hexShortToInt(fullData.substring(currentImageStartIndex + 8, currentImageStartIndex + 12)),
      hotY: bytetools.hexShortToInt(fullData.substring(currentImageStartIndex + 12, currentImageStartIndex + 16)),
      actX: bytetools.hexShortToInt(fullData.substring(currentImageStartIndex + 16, currentImageStartIndex + 20)),
      actY: bytetools.hexShortToInt(fullData.substring(currentImageStartIndex + 20, currentImageStartIndex + 24)),
      data: (fullData.substring(indexData, indexData + (lll * 2)))
    };
  }
  catch (e) {
    console.log('IMAGE CONTAINS GARBAGE.');
    console.log('Full Data Length: ' + fullData.length);
    console.log('Current image start index: ' + currentImageStartIndex);
    console.log('Width String: ' + fullData.substring(currentImageStartIndex, currentImageStartIndex + 4));
    throw e;
  }
  return img;
}

function createImgObjAuto(imgId) {
  var idxOfIndStart = 2 * (OFFSETS_START + imgId * 4);
  var indStart = 2 * bytetools.hexIntToInt(fullData.substring(idxOfIndStart,
                                                              idxOfIndStart+8));
  var indexOfDataSizeInt = indStart + 24;
  var indexData = indexOfDataSizeInt + 8;
  var lll = Number.parseInt(bytetools.hexIntToInt(fullData.substring(indexOfDataSizeInt, indexData)));
  var targetString = null;
  return (createImgObj(indStart, indexOfDataSizeInt, indexData, lll, targetString, imgId));
}

function writeImageToFile(img, rawPixelBuffer, version) {
  try {
    console.log(img.imgId);
    img.filename = pathImageOut + img.imgId + imgExt;
    var png = new Jimp(img.width, img.height);
    png.imgObj = img;
    png.bitmap.data = rawPixelBuffer;
    pngs.push(png);   
    //pngs[pngs.length -1].write(img.filename, function () {/*console.log('Wrote File...');*/});
  }
  catch (e) {
    console.log(e);
    console.log('@@@Inflation burst. Aborting image dump.');
  }
}

function createPath(path) {
  try {
    fs.accessSync(path, fs.F_OK);
    // Do something
  } catch (e) {
    console.log('Could not find path "' + path + '". Attempting to create it.');
    fs.mkdirSync(path);
  }
}

function startProcessingPngs() {
  console.log('Writing img ' + pngs[pngsIndex].imgObj.filename);
  pngs[pngsIndex].write(pngs[pngsIndex].imgObj.filename, processNextPng);
}

function processNextPng() {
  pngsIndex += 1;
  if (pngsIndex < pngs.length) {
    console.log('Writing img ' + pngs[pngsIndex].imgObj.filename);
    pngs[pngsIndex].write(pngs[pngsIndex].imgObj.filename, processNextPng);
  }
}

main();
