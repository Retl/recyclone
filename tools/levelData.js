var fs = require('fs');

LevelData = function () {
	version = "Unknown";
};

/*
LevelData.prototype.identifyVersion = function (filename) {
	var version = "Unknown";
	var stats = fs.statSync(filename);
	var fileSizeInBytes = stats["size"];
	console.log("Filezie is: " + fileSizeInBytes);
	switch (fileSizeInBytes) {
		case '31688192':
			version = "STEAM-1.20.4";
			break;
		default:
			version = "Unknown"; 
			break;
			
			this.initVersion(version);
			return(version);
	}
};*/

LevelData.prototype.initVersion = function (version) {
	this.version = version;
	// Do the rest of the data for object signatures and whatnot, I suppose.
};

module.exports.LevelData = LevelData;
module.exports.identifyVersion = LevelData.prototype.identifyVersion;
module.exports.initVersion = LevelData.prototype.initVersion;